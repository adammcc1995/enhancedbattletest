﻿using System;
using TaleWorlds.Library;

namespace EnhancedBattleTest
{
    public class EnhancedCustomBattleConfigVM : BattleConfigVMBase<EnhancedCustomBattleConfig>
    {
        private Action<EnhancedCustomBattleConfig> startAction;
        private Action<EnhancedCustomBattleConfig> backAction;
        
        private int _selectedSceneIndex;
        private string _skyBrightness;
        private string _rainDensity;

        private string _selectedMapName;

        [DataSourceProperty]
        public string SelectedMapName
        {
            get => this._selectedMapName;
            set
            {
                this._selectedMapName = value;
                this.OnPropertyChanged(nameof(SelectedMapName));
            }
        }

        public int SelectedSceneIndex
        {
            get => this._selectedSceneIndex;
            set
            {
                if (value < 0 || value >= CurrentConfig.sceneList.Length || value == this._selectedSceneIndex)
                    return;

                this.CurrentConfig.sceneIndex = value;
                this._selectedSceneIndex = value;
                UpdateSceneContent();
            }
        }

        [DataSourceProperty]
        public string SkyBrightness
        {
            get => this._skyBrightness;
            set
            {
                if (this._skyBrightness == value)
                    return;
                this._skyBrightness = value;
                this.OnPropertyChanged(nameof(SkyBrightness));
            }
        }

        [DataSourceProperty]
        public string RainDensity
        {
            get => _rainDensity;
            set
            {
                if (this._rainDensity == value)
                    return;
                this._rainDensity = value;
                this.OnPropertyChanged(nameof(RainDensity));
            }
        }

        public EnhancedCustomBattleConfigVM(CharacterSelectionView selectionView, Action<EnhancedCustomBattleConfig> startAction,
            Action<EnhancedCustomBattleConfig> backAction)
            : base(selectionView, EnhancedCustomBattleConfig.Get())
        {
            InitializeContent();

            this.startAction = startAction;
            this.backAction = backAction;
        }

        private void PreviousMap()
        {
            if (this.SelectedSceneIndex == 0)
                return;
            this.SelectedSceneIndex--;
        }
        private void NextMap()
        {
            if (this.SelectedSceneIndex + 1 >= CurrentConfig.sceneList.Length)
                return;
            this.SelectedSceneIndex++;
        }

        private new void SelectPlayerCharacter()
        {
            base.SelectPlayerCharacter();
        }

        private new void SelectEnemyCharacter()
        {
            base.SelectEnemyCharacter();
        }

        private new void SelectPlayerTroopCharacter1()
        {
            base.SelectPlayerTroopCharacter1();
        }

        private new void SelectPlayerTroopCharacter2()
        {
            base.SelectPlayerTroopCharacter2();
        }

        private new void SelectPlayerTroopCharacter3()
        {
            base.SelectPlayerTroopCharacter3();
        }

        private new void SelectEnemyTroopCharacter1()
        {
            base.SelectEnemyTroopCharacter1();
        }

        private new void SelectEnemyTroopCharacter2()
        {
            base.SelectEnemyTroopCharacter2();
        }

        private new void SelectEnemyTroopCharacter3()
        {
            base.SelectEnemyTroopCharacter3();
        }

        private void Start()
        {
            if (SaveConfig() != SaveParamResult.success)
                return;
            ModuleLogger.Writer.WriteLine("StartBattle");
            this.startAction(CurrentConfig);
        }

        private void Save()
        {
            SaveConfig();
        }

        private void LoadConfig()
        {
            this.CurrentConfig.ReloadSavedConfig();
            this.InitializeContent();
            Utility.DisplayMessage("Reset successfully");
        }

        public void GoBack()
        {
            backAction(this.CurrentConfig);
        }

        private void InitializeContent()
        {
            this._selectedSceneIndex = CurrentConfig.sceneIndex;
            UpdateSceneContent();
        }

        private void UpdateSceneContent()
        {
            this.SelectedMapName = CurrentConfig.SceneName;
            this.SkyBrightness = CurrentConfig.SkyBrightness.ToString();
            this.RainDensity = CurrentConfig.RainDensity.ToString();
        }

        protected override void ApplyConfig()
        {
            base.ApplyConfig();
            CurrentConfig.sceneIndex = this.SelectedSceneIndex;
            CurrentConfig.SkyBrightness = System.Convert.ToSingle(this.SkyBrightness);
            CurrentConfig.RainDensity = System.Convert.ToSingle(this.RainDensity);
            
        }
    }

}